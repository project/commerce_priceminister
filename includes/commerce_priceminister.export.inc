<?php
/**
 * @file
 *
 * Export Commerce Products to Priceminister.
 */

/**
 * Performs long action to Priceminister via their webservices.
 *
 * This batch is used for checking the status of products that have been
 * exported and for exporting new products.
 *
 * @param CommercePriceministerFeed $feed
 *   A feed object
 */
function commerce_priceminister_export_feed_batch($feed) {
  // Allow products to be requeued even if their are currently pushed to
  // Priceminister.
  $max_items = 50;
  $operations = array();

  $api = new PriceministerAPI();
  $operations = array();

  $operations[] = array('commerce_priceminister_check_export_batch', array($feed, $api));

  // Process the queue to get the list of products to export.
  $products_queue = DrupalQueue::get('priceminister_product_feed_' . $feed->id);
  while ($products_chunks = $products_queue->claimItems($max_items)) {
    $operations[] = array('commerce_priceminister_export_product_batch', array($feed, $api, $products_chunks));
  }

  // Prepares the batches.
  $batch = array(
    'title' => t('Exporting products to Priceminister'),
    'operations' => $operations,
    'finished' => 'commerce_priceminister_queue_finished',
    'file' => drupal_get_path('module', 'commerce_priceminister') . '/includes/commerce_priceminister.export.inc',
  );
  batch_set($batch);
  batch_process();
}

/**
 * Batch wrapper for exporting products().
 *
 * @param CommercePriceministerFeed $feed
 *   A feed object
 * @param PriceministerAPI $api
 *   A Priceminister API object to communicate with Priceminister
 */
function commerce_priceminister_check_export_batch(CommercePriceministerFeed $feed, PriceministerAPI $api) {
  commerce_priceminister_check_exported_feed($feed, $api);
}

/**
 * Checks the status of products exported previously.
 *
 * When a product is sent to Priceminister, they send us a file_id to use for
 * checking the status of the export later that we store it in the queue.
 * This function will check the status of every file_id received for a feed.
 *
 * @param CommercePriceministerFeed $feed
 *   A feed object
 * @param PriceministerAPI $api
 *   A Priceminister API object to communicate with Priceminister
 *
 * @return array
 */
function commerce_priceminister_check_exported_feed(CommercePriceministerFeed $feed, PriceministerAPI $api) {

  // Get the queue where
  $queued_files = DrupalQueue::get('priceminister_files_feed_' . $feed->id);

  $files_to_release = array();

  // Process every file items in the queue.
  while ($item = $queued_files->claimItem()) {
    $export_report = $api->getExportReport($feed->id, $item);
    // Log reports informations.
    $feed_report = commerce_priceminister_create_report_feed($export_report['report']);
    if (isset($item->data['report_id'])) {
      $feed_report['id'] = $item->data['report_id'];
      unset($feed_report['created']);
    }
    commerce_priceminister_add_report_feed($feed_report);

    if (!isset($item->data['report_id']) && isset($feed_report['id'])) {
      $item->data['report_id'] = $feed_report['id'];
      $queued_files->deleteItem($item);
      $queued_files->createItem($item->data);
    }
    if ($export_report['return']) {
      foreach ($export_report['errors'] as $product) {
        $report = commerce_priceminister_create_report_product(array(
          'report_id' => $feed_report['id'],
          'feed_id' => $feed->id,
          'sku' => $product['sku'],
          'data' => $product['errors'],
        ));
        commerce_priceminister_add_report_product($report);
      }
      // Remove the file from the queue if the export was ok.
      $queued_files->deleteItem($item);
    }
    else {
      // Marks the file to be added again to the queue.
      $files_to_release[] = $item;
    }
  }
  // Adds products to the queue if the export failed.
  foreach ($files_to_release as $file) {
    $queued_files->releaseItem($file);
  }

  return array();
}

/**
 * Batch wrapper for exporting product.
 *
 * @param CommercePriceministerFeed $feed
 *   A feed object
 * @param PriceministerAPI $api
 *   A Priceminister API object to communicate with Priceminister
 * @param $products
 *   Products to export.
 */
function commerce_priceminister_export_product_batch(CommercePriceministerFeed $feed, PriceministerAPI $api, $products) {
  if (!isset($context['results']['data'])) {
    $context['results']['data'] = array(
      'products_pending' => array(),
      'products_requeued' => array(),
    );
  }

  $results = commerce_priceminister_export_product($feed, $api, $products);

  $context['results']['data'] = array_merge_recursive($context['results']['data'], $results);
}

/**
 * Exports queued products.
 *
 * Generate an XML file and send it to Priceminister. The returned file of the
 * export will be stored in order to check if the export was good or not.
 *
 * @param CommercePriceministerFeed $feed
 *   A feed object
 * @param PriceministerAPI $api
 *   A Priceminister API object to communicate with Priceminister
 * @param $products_items
 *   Products to export.
 *
 * @return array
 */
function commerce_priceminister_export_product(CommercePriceministerFeed $feed, PriceministerAPI $api, $products_items) {

  if (empty($products_items)) {
    return;
  }

  $results = array(
    'products_pending' => array(),
    'products_requeued' => array(),
  );

  $products_queue = DrupalQueue::get('priceminister_product_feed_' . $feed->id);
  $files_queue = DrupalQueue::get('priceminister_files_feed_' . $feed->id);

  // Process queue
  $product_skus = array();
  foreach ($products_items as $products_item) {
    $product_skus[$products_item->item_id] = $products_item->data;
  }
  $products = commerce_product_load_multiple(FALSE, array('sku' => $product_skus));

  // Generate items
  $priceminister_products_metadata = commerce_priceminister_product_metadata_load_mutiple(FALSE, array('product_id' => array_keys($products)));
  foreach ($priceminister_products_metadata as $product_metadata) {
    $products_metadata[$product_metadata->product_id] = $product_metadata;
  }
  $items = array();
  foreach ($products as $product) {
    $product_metadata = isset($products_metadata[$product->product_id]) ? $products_metadata[$product->product_id] : NULL;
    $item = commerce_priceminister_generate_product_to_export($product, $product_metadata, $feed);
    if (_commerce_priceminister_is_item_exportable($item, $feed, $product, $product_metadata)) {
      $items[$product->sku] = $item;
    }
    else {
      $products_queue->deleteItem($products_item);
      // TODO: return & log errors from _commerce_priceminister_is_item_exportable
    }
  }

  // Send to api
  $file = $api->exportProducts($items);
  if (!empty($file['file_id']) && $file['status'] == 'OK') {
    // Store in files queue (file_id, items_ids)
    $item = array(
      'file_id' => $file['file_id'],
      'product_skus' => $file['product_skus'],
    );
    // TODO: feed stats
    if ($files_queue->createItem($item)) {
      $products_queue->deleteItems(array_keys($products_items));
      $results['products_pending'] = $file['product_skus'];
      $file['product_skus'] = array();
    }
  }
  // Something failed, release items.
  if (!empty($file['product_skus'])) {
    $results['products_requeued'] = $file['product_skus'];
    $products_queue->releaseItems(array_keys($products_items));
  }

  return $results;
}

/**
 * Batch finished callback.
 *
 * Aggregate results and diplays them.
 */
function commerce_priceminister_queue_finished($success, $results, $operations, $elapsed) {
  $products_pending = isset($results['data']['products_pending']) ? count($results['data']['products_pending']) : 0;
  $products_requeued = isset($results['data']['products_requeued']) ? count($results['data']['products_requeued']) : 0;
  $message = NULL;
  if ($products_pending) {
    $message .= format_plural($products_pending, t('1 product has successfully been sent to Priceminister and is waiting to be processed.'), t('@count products have successfully been sent to Priceminister and are waiting to be processed.'));
  }
  if ($products_pending && $products_requeued) {
    $message .= t('and') . '<br />';
  }
  if ($products_requeued) {
    $message .= format_plural($products_requeued, t("1 product wasn't sent to Priceminister and will be re-processed later."), t("@count products weren't sent to Priceminister and will be re-processed later."));
  }

  drupal_set_message($message);
}

/**
 * Generate an product item as waited from Priceminister.
 *
 * TODO - Change the way to manage special field mapping such as price. Use
 * function field_$entity_$entity_field if exist.
 *
 * @param $product
 * @param null $product_metadata
 * @param $feed
 * @return array
 */
function commerce_priceminister_generate_product_to_export($product, $product_metadata, $feed) {
  foreach ($feed->getPriceministerProductTypeFields() as $key => $field) {
    $value = NULL;
    // If the value has to be found in a field of an entity.
    if (isset($feed->fields_mapping[$key]['local']['field'])) {
      $entity = key($feed->fields_mapping[$key]['local']['field']);
      $entity_field = reset($feed->fields_mapping[$key]['local']['field']);
      // If the value is located in the commere_product entity.
      if ($entity == 'commerce_product') {
        $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
        if ($entity_field == 'commerce_price') {
          $price = $product_wrapper->commerce_price->value();
          $value = commerce_currency_amount_to_decimal($price['amount'], 'EUR');
        }
        else {
          $value = $product->{$entity_field};
        }
      }
      // If the value is located in the commere_priceminister_metadata_product
      // entity.
      elseif ($product_metadata && $entity == 'commerce_priceminister_product_metadata') {
        $value = isset($product_metadata->metadata[$entity_field]) ? $product_metadata->metadata[$entity_field] : NULL;
      }
    }
    // If the value is just a default value added by the merchant.
    elseif (isset($feed->fields_mapping[$key]['local']['value'])) {
      $value = $feed->fields_mapping[$key]['local']['value'];
    }

    // Apply specifique output condition on some kind of field.
    if ($value) {
      if ($field['key'] == 'image_url') {
        if ($image = file_load($value)) {
          $style = isset($product_metadata->metadata['image_style_name']) ? $product_metadata->metadata['image_style_name'] : '';
          $value = image_style_url($style, $image->uri);
        }
      }
      elseif ($field['key'] == 'state') {
        $state_values = array(
          0  => 'New',
          10 => 'As new',
          20 => 'Very good',
          30 => 'Good',
          40 => 'Acceptable',
          50 => 'Out of order',
        );
        $state_values = array_flip($state_values);
        $value = $state_values[$product_metadata->metadata['state']];
      }
      elseif ($field['valuetype'] == 'Number') {
        $value = (int) $value;
      }
      elseif ($field['valuetype'] == 'Boolean') {
        $value = (bool) $value;
      }
      $attributes[$field['parent']][$key] = $value;
    }
  }

  $item = array(
    'alias' => $feed->priceminister_product_type,
    'attributes' => $attributes
  );
  return $item;
}