<?php

/**
 * @file
 */


/**
 * The Controller for product entities
 */
class CommercePriceministerProductMetadataEntityAPIController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create a default CommerceMarketplaceFeed entity.
   *
   * @param array $values
   *   An array of defaults values to add to the object construction.
   * @return object
   */
  public function create(array $values = array()) {
    global $user;
    $values += array(
      'is_new' => TRUE,
      'id' => NULL,
      'product_id' => NULL,
      'metadata' => array(),
      //'status' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );
    return parent::create($values);
  }

  /**
   * Saves a feed.
   *
   *
   * @param object $priceminister_product
   *   The full object to save.
   * @param DatabaseTransaction $db_transaction
   *   An optional transaction object.
   *
   * @return bool|int
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   *
   * @throws Exception
   */
  public function save($priceminister_product, DatabaseTransaction $db_transaction = NULL) {
    if (!isset($db_transaction)) {
      $db_transaction = db_transaction();
      $started_transaction = TRUE;
    }

    try {
      // Determine if we will be inserting a new feed.
      $priceminister_product->is_new = empty($priceminister_product->id);
      // Set the timestamp fields.
      if (empty($priceminister_product->created)) {
        $priceminister_product->created = REQUEST_TIME;
      }
      else {
        if ($priceminister_product->created === '') {
          unset($priceminister_product->created);
        }
      }

      $priceminister_product->changed = REQUEST_TIME;

      return parent::save($priceminister_product, $db_transaction);
    }
    catch (Exception $e) {
      if (!empty($started_transaction)) {
        $db_transaction->rollback();
        watchdog_exception($this->entityType, $e);
      }
      throw $e;
    }
  }
}


class CommercePriceministerProductMetadata extends Entity {

  public function __construct(array $values = array()) {
    parent::__construct($values, 'commerce_priceminister_product_metadata');
  }
}