<?php

/**
 * @param $product
 * @return array|mixed
 */
function commerce_marketplace_product_metadata_wrapper($product) {
  // Load the commerce_priceminister_product_metadata associated to the
  // commerce_product.
  $priceminister_product = commerce_priceminister_product_metadata_load_by_product_id($product->product_id);

  // Creates a new one if it doesn't exist yet.
  if (empty($priceminister_product)) {
    $priceminister_product = commerce_priceminister_product_metadata_create(array('product_id' => $product->product_id));
  }
  return drupal_get_form('commerce_marketplace_product_metadata_form', $product, $priceminister_product);
}


/**
 * @param $form
 * @param $form_state
 * @param $product
 * @param $product_metadata
 * @return array
 */
function commerce_marketplace_product_metadata_form($form, &$form_state, $product, $product_metadata) {
  $form = array();
  $form_state['product'] = $product;
  $form_state['priceminister_product'] = $product_metadata;

  // Gets the list of Priceminister's products mapped with the current product
  // type.
  $priceminister_product_type = commerce_priceminister_get_priceminister_mapped_product_types($product->type);
  if (empty($priceminister_product_type)) {
    return array(
      '#markup' => t('This product is not mapped with Priceminister.<br />If you want to export this product and this product category go to the Priceminister feed interface to manage it.')
    );
  }
  // Converts Priceminister's field to Drupal.
  $form = _commerce_priceminister_generate_product_metadata_fields($priceminister_product_type, $product_metadata);

  $form['description'] = array(
    '#markup' => '<p>' . t("Saved values will be send to Priceminister's export.") . '</p>',
    '#weight' => -500,
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#weight' => 500,
  );
  $form['actions']['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'admin/commerce/config/marketplaces/priceminister/feeds'),
    '#weight' => 501,
  );

  return $form;
}


/**
 * Generate fields for a Priceminister product type
 *
 * @param $product_metadata
 * @return mixed
 */
function _commerce_priceminister_generate_product_metadata_fields($priceminister_product_type, $product_metadata) {
  $fields = commerce_priceminister_get_product_metadata_fields($priceminister_product_type);
  if (empty($fields)) {
    return;
  }

  $i = 0;
  $hidden_fields = array_keys(CommercePriceministerFeed::getRequiredHiddenFields());
  foreach ($fields as $key => $field) {
    if (in_array($key, $hidden_fields)) {
      continue;
    }
    $field_value = isset($product_metadata->metadata[$key]) ? $product_metadata->metadata[$key] : '';
    // Image field.
    if ($field['key'] == 'image_url') {
      $image = isset($product_metadata->metadata[$key]) ? file_load($product_metadata->metadata[$key]) : NULL;
      $image_style = isset($product_metadata->metadata['image_style']) ? $product_metadata->metadata['image_style'] : NULL;
      $form[$key] = _commerce_priceminister_generate_field_image($key, $image, $image_style);
    }
    // Select field.
    elseif (isset($field['valueslist'])) {
      $form[$key] = _commerce_priceminister_generate_field_select($field, $field_value);
    }
    // Text field.
    elseif ($field['valuetype'] == 'Text' || $field['valuetype'] == 'Number') {
      $form[$key] = _commerce_priceminister_generate_field_textfield($field, $field_value);
    }
    // Checkbox field.
    elseif ($field['valuetype'] == 'Boolean') {
      $form[$key] = _commerce_priceminister_generate_field_boolean($field, $field_value);
    }
    // Date field.
    elseif ($field['valuetype'] == 'Date') {
      $form[$key] = _commerce_priceminister_generate_field_date($field, $field_value);
    }
    $form[$key]['#description'] .= t('This field is used to map with %product_types Priceminister product type', array('%product_types' => implode(', ', $field['product_types'])));
    $form[$key]['#weight'] = ($field['mandatory'] == TRUE) ? ((100 - $i) * -1) : $i;
  }
  return $form;
}

function _commerce_priceminister_generate_field_default($field, $value) {
  return array(
    '#title' => check_plain($field['label']),
    '#description' => '',
    '#default_value' => $value,
  );
}


function _commerce_priceminister_generate_field_select($field, $value = NULL) {
  $return = array(
    '#type' => 'select',
    '#options' => array('' => t('- Select -')) + $field['valueslist'],
  );
  $return += _commerce_priceminister_generate_field_default($field, $value);
  return $return;
}

/**
 * @param $field
 * @param null $value
 * @return array
 */
function _commerce_priceminister_generate_field_textfield($field, $value = NULL) {
  $return = array(
    '#type' => 'textfield',
  );
  $return += _commerce_priceminister_generate_field_default($field, $value);
  return $return;
}

/**
 * @param $field
 * @param null $value
 * @return array
 */
function _commerce_priceminister_generate_field_date($field, $value = NULL) {
  if (module_exists('date')) {
    // TODO : Add support for the date module.
  }

  $return = array(
    '#type' => 'textfield',
    '#description' => t('Date format must be : DD/MM/YYYY <br/>'),
  );
  $return += _commerce_priceminister_generate_field_default($field, $value);
  return $return;
}

/**
 * @param $field
 * @param null $value
 * @return array
 */
function _commerce_priceminister_generate_field_boolean($field, $value = NULL) {
  $return = array(
    '#type' => 'checkbox',
  );
  $return += _commerce_priceminister_generate_field_default($field, $value);
  return $return;
}

function _commerce_priceminister_generate_field_image($field_id, $image, $image_style) {
  if ($image) {
    $fields['image_preview'] = array(
      '#markup' => theme('image_style', array(
        'style_name' => 'thumbnail',
        'path' => $image->uri,
        )
      ),
    );
  }
  $fields[$field_id] = array(
    '#title' => t('Image'),
    '#type' => 'managed_file',
    '#description' => t('The uploaded image will be displayed on this page using the image style choosen below.'),
    //'#upload_location' => 'public://commerce_priceminsiter_product_metadata',
    '#upload_location' => 'public://uploads',
    '#default_value' => isset($image->fid) ? $image->fid : '',
  );
  $fields['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#description' => t('Choose an image style to use when displaying this image.'),
    '#options' => image_style_options(TRUE),
    '#default_value' => $image_style,
  );
  $fields['#description']= '';
  return $fields;
}

/**
 * @param $form
 * @param $form_state
 */
function commerce_marketplace_product_metadata_form_submit($form, &$form_state) {
  $product = $form_state['product'];

  $priceminister_product = $form_state['priceminister_product'];
  form_state_values_clean($form_state);
  foreach ($form_state['values'] as $key => $value) {
    $metadata[$key] = $value;
  }
  $priceminister_product->metadata = array_filter($metadata);

  commerce_priceminister_product_metadata_save($priceminister_product);
}