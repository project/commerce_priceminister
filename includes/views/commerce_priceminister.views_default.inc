<?php

/**
 * Implements hook_views_default_views().
 */
function commerce_priceminister_views_default_views() {
  $view = new view();
  $view->name = 'priceminister_products';
  $view->description = 'Display a list of exportable products for priceminister API.';
  $view->tag = 'commerce';
  $view->base_table = 'commerce_product';
  $view->human_name = 'Priceminister products';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['title'] = 'Products';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer commerce_product entities';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = 50;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'sku' => 'sku',
    'title' => 'title',
    'commerce_price' => 'commerce_price',
    'status' => 'status',
    'id' => 'id',
    'priceminister_exportable' => 'priceminister_exportable',
    'operations' => 'operations',
    'type' => 'type',
  );
  $handler->display->display_options['style_options']['default'] = 'sku';
  $handler->display->display_options['style_options']['info'] = array(
    'sku' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'commerce_price' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'id' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'priceminister_exportable' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'All product of type [type] exportable on this Priceminister Feed.';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  $handler->display->display_options['header']['area']['tokenize'] = TRUE;
  /* No results behavior: Commerce Product: Empty text */
  $handler->display->display_options['empty']['empty_text']['id'] = 'empty_text';
  $handler->display->display_options['empty']['empty_text']['table'] = 'commerce_product';
  $handler->display->display_options['empty']['empty_text']['field'] = 'empty_text';
  $handler->display->display_options['empty']['empty_text']['add_path'] = 'admin/commerce/products/add';
  /* Field: Commerce Product: SKU */
  $handler->display->display_options['fields']['sku']['id'] = 'sku';
  $handler->display->display_options['fields']['sku']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['sku']['field'] = 'sku';
  $handler->display->display_options['fields']['sku']['link_to_product'] = 0;
  /* Field: Commerce Product: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['link_to_product'] = 1;
  /* Field: Commerce Product: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['link_to_product'] = 0;
  /* Field: Commerce Product: Price */
  $handler->display->display_options['fields']['commerce_price']['id'] = 'commerce_price';
  $handler->display->display_options['fields']['commerce_price']['table'] = 'field_data_commerce_price';
  $handler->display->display_options['fields']['commerce_price']['field'] = 'commerce_price';
  $handler->display->display_options['fields']['commerce_price']['click_sort_column'] = 'amount';
  $handler->display->display_options['fields']['commerce_price']['type'] = 'commerce_price_formatted_amount';
  /* Field: Commerce Product: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['type'] = 'active-disabled';
  $handler->display->display_options['fields']['status']['not'] = 0;
  /* Field: Commerce Product: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['label'] = 'Operations';
  $handler->display->display_options['fields']['operations']['add_destination'] = 1;
  /* Sort criterion: Commerce Product: SKU */
  $handler->display->display_options['sorts']['sku']['id'] = 'sku';
  $handler->display->display_options['sorts']['sku']['table'] = 'commerce_product';
  $handler->display->display_options['sorts']['sku']['field'] = 'sku';
  /* Filter criterion: Commerce Product: SKU */
  $handler->display->display_options['filters']['sku']['id'] = 'sku';
  $handler->display->display_options['filters']['sku']['table'] = 'commerce_product';
  $handler->display->display_options['filters']['sku']['field'] = 'sku';
  $handler->display->display_options['filters']['sku']['operator'] = 'contains';
  $handler->display->display_options['filters']['sku']['exposed'] = TRUE;
  $handler->display->display_options['filters']['sku']['expose']['operator_id'] = 'sku_op';
  $handler->display->display_options['filters']['sku']['expose']['label'] = 'Filter by SKUs containing';
  $handler->display->display_options['filters']['sku']['expose']['operator'] = 'sku_op';
  $handler->display->display_options['filters']['sku']['expose']['identifier'] = 'sku';

  /* Display: default */
  $handler = $view->new_display('page', 'default', 'priceminister');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Commerce Product: Priceminister feed <- product */
  $handler->display->display_options['relationships']['priceminister_type']['id'] = 'priceminister_type';
  $handler->display->display_options['relationships']['priceminister_type']['table'] = 'commerce_product';
  $handler->display->display_options['relationships']['priceminister_type']['field'] = 'priceminister_type';
  $handler->display->display_options['relationships']['priceminister_type']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Commerce Product: SKU */
  $handler->display->display_options['fields']['sku']['id'] = 'sku';
  $handler->display->display_options['fields']['sku']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['sku']['field'] = 'sku';
  $handler->display->display_options['fields']['sku']['link_to_product'] = 0;
  /* Field: Commerce Product: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['link_to_product'] = 1;
  /* Field: Commerce Product: Price */
  $handler->display->display_options['fields']['commerce_price']['id'] = 'commerce_price';
  $handler->display->display_options['fields']['commerce_price']['table'] = 'field_data_commerce_price';
  $handler->display->display_options['fields']['commerce_price']['field'] = 'commerce_price';
  $handler->display->display_options['fields']['commerce_price']['click_sort_column'] = 'amount';
  $handler->display->display_options['fields']['commerce_price']['type'] = 'commerce_price_formatted_amount';
  /* Field: Commerce Product: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['type'] = 'active-disabled';
  $handler->display->display_options['fields']['status']['not'] = 0;
  /* Field: Commerce Priceminister Feed: Priceminister feed id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'commerce_priceminister_feeds';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['relationship'] = 'priceminister_type';
  $handler->display->display_options['fields']['id']['label'] = '';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
  /* Field: Commerce Product: Priceminister product is exportable */
  $handler->display->display_options['fields']['priceminister_exportable']['id'] = 'priceminister_exportable';
  $handler->display->display_options['fields']['priceminister_exportable']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['priceminister_exportable']['field'] = 'priceminister_exportable';
  $handler->display->display_options['fields']['priceminister_exportable']['label'] = 'Exportable';
  /* Field: Commerce Product: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['label'] = 'Operations';
  $handler->display->display_options['fields']['operations']['add_destination'] = 1;
  /* Field: Commerce Product: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['type']['link_to_product'] = 0;
  $handler->display->display_options['fields']['type']['use_raw_value'] = 0;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Commerce Priceminister Feed: Priceminister feed id */
  $handler->display->display_options['arguments']['id']['id'] = 'id';
  $handler->display->display_options['arguments']['id']['table'] = 'commerce_priceminister_feeds';
  $handler->display->display_options['arguments']['id']['field'] = 'id';
  $handler->display->display_options['arguments']['id']['relationship'] = 'priceminister_type';
  $handler->display->display_options['arguments']['id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['path'] = 'admin/commerce/config/marketplaces/priceminister/feed/%/product';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Products';
  $handler->display->display_options['menu']['weight'] = '30';
  $handler->display->display_options['menu']['context'] = 0;

  // Add view to list of views to provide.
  $views[$view->name] = $view;

  return $views;
}

/**
 * Implements hook_views_default_views_alter().
 */
function commerce_priceminister_views_default_views_alter(&$views) {
  // Add the Priceminister admin view to manage orders.
  if (isset($views['commerce_orders']) || isset($views['commerce_backoffice_orders'])) {
    /* Display: Priceminister */
    $handler = $views[isset($views['commerce_backoffice_orders']) ? 'commerce_backoffice_orders' : 'commerce_orders']->new_display('page', 'Priceminister', 'priceminister');
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'table';
    $handler->display->display_options['style_options']['columns'] = array(
      'order_number' => 'order_number',
      'created' => 'created',
      'priceminister_username' => 'priceminister_username',
      'commerce_order_total' => 'commerce_order_total',
      'status' => 'status',
      'operations' => 'operations',
    );
    $handler->display->display_options['style_options']['default'] = '-1';
    $handler->display->display_options['style_options']['info'] = array(
      'order_number' => array(
        'sortable' => 0,
        'default_sort_order' => 'desc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'created' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'priceminister_username' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'commerce_order_total' => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'status' => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'operations' => array(
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
    );
    $handler->display->display_options['style_options']['empty_table'] = TRUE;
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['fields'] = FALSE;
    /* Field: Commerce Order: Order number */
    $handler->display->display_options['fields']['order_number']['id'] = 'order_number';
    $handler->display->display_options['fields']['order_number']['table'] = 'commerce_order';
    $handler->display->display_options['fields']['order_number']['field'] = 'order_number';
    $handler->display->display_options['fields']['order_number']['link_to_order'] = 'admin';
    /* Field: Commerce Order: Created date */
    $handler->display->display_options['fields']['created']['id'] = 'created';
    $handler->display->display_options['fields']['created']['table'] = 'commerce_order';
    $handler->display->display_options['fields']['created']['field'] = 'created';
    $handler->display->display_options['fields']['created']['date_format'] = 'medium';
    /* Field: Commerce Order: Username */
    $handler->display->display_options['fields']['priceminister_username']['id'] = 'priceminister_username';
    $handler->display->display_options['fields']['priceminister_username']['table'] = 'field_data_priceminister_username';
    $handler->display->display_options['fields']['priceminister_username']['field'] = 'priceminister_username';
    $handler->display->display_options['fields']['priceminister_username']['label'] = 'Priceminister username';
    /* Field: Commerce Order: Order total */
    $handler->display->display_options['fields']['commerce_order_total']['id'] = 'commerce_order_total';
    $handler->display->display_options['fields']['commerce_order_total']['table'] = 'field_data_commerce_order_total';
    $handler->display->display_options['fields']['commerce_order_total']['field'] = 'commerce_order_total';
    $handler->display->display_options['fields']['commerce_order_total']['label'] = 'Total';
    $handler->display->display_options['fields']['commerce_order_total']['click_sort_column'] = 'amount';
    $handler->display->display_options['fields']['commerce_order_total']['type'] = 'commerce_price_formatted_amount';
    $handler->display->display_options['fields']['commerce_order_total']['settings'] = array(
      'calculation' => FALSE,
    );
    /* Field: Commerce Order: Order status */
    $handler->display->display_options['fields']['status']['id'] = 'status';
    $handler->display->display_options['fields']['status']['table'] = 'commerce_order';
    $handler->display->display_options['fields']['status']['field'] = 'status';
    /* Field: Commerce Order: Operations links */
    $handler->display->display_options['fields']['operations']['id'] = 'operations';
    $handler->display->display_options['fields']['operations']['table'] = 'commerce_order';
    $handler->display->display_options['fields']['operations']['field'] = 'operations';
    $handler->display->display_options['fields']['operations']['label'] = 'Operations';
    $handler->display->display_options['defaults']['filter_groups'] = FALSE;
    $handler->display->display_options['defaults']['filters'] = FALSE;
    /* Filter criterion: Commerce Order: Order type */
    $handler->display->display_options['filters']['type']['id'] = 'type';
    $handler->display->display_options['filters']['type']['table'] = 'commerce_order';
    $handler->display->display_options['filters']['type']['field'] = 'type';
    $handler->display->display_options['filters']['type']['value'] = array(
      'priceminister' => 'priceminister',
    );
    $handler->display->display_options['path'] = 'admin/commerce/config/marketplaces/priceminister/orders';
    $handler->display->display_options['menu']['type'] = 'tab';
    $handler->display->display_options['menu']['title'] = 'Orders';
    $handler->display->display_options['menu']['weight'] = '1';
    $handler->display->display_options['menu']['name'] = 'management';
    $handler->display->display_options['menu']['context'] = 0;
    $handler->display->display_options['menu']['context_only_inline'] = 0;
  }

  // Add the Priceminister admin block to accept/refuse line items.
  if (isset($views['commerce_line_item_table'])) {
    /* Display: Block */
    $handler = $views['commerce_line_item_table']->new_display('block', 'Block', 'priceminister');
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'table';
    $handler->display->display_options['style_options']['columns'] = array(
      'line_item_id' => 'line_item_id',
      'type' => 'type',
      'line_item_title' => 'line_item_title',
      'line_item_label' => 'line_item_title',
      'commerce_unit_price' => 'commerce_unit_price',
      'quantity' => 'quantity',
      'commerce_total' => 'commerce_total',
      'priceminister_transporter_name' => 'priceminister_transporter_name',
      'priceminister_tracking_number' => 'priceminister_transporter_name',
      'priceminister_tracking_url' => 'priceminister_transporter_name',
      'priceminister_actions' => 'priceminister_actions',
    );
    $handler->display->display_options['style_options']['default'] = '-1';
    $handler->display->display_options['style_options']['info'] = array(
      'line_item_id' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'type' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'line_item_title' => array(
        'align' => '',
        'separator' => ' ',
        'empty_column' => 0,
      ),
      'line_item_label' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'commerce_unit_price' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'quantity' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'commerce_total' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => 'views-align-right',
        'separator' => '',
        'empty_column' => 0,
      ),
      'priceminister_transporter_name' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => ', ',
        'empty_column' => 0,
      ),
      'priceminister_tracking_number' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'priceminister_tracking_url' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'priceminister_actions' => array(
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
    );
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['relationships'] = FALSE;
    /* Relationship: Commerce Line Item: Order ID */
    $handler->display->display_options['relationships']['order_id']['id'] = 'order_id';
    $handler->display->display_options['relationships']['order_id']['table'] = 'commerce_line_item';
    $handler->display->display_options['relationships']['order_id']['field'] = 'order_id';
    $handler->display->display_options['relationships']['order_id']['required'] = TRUE;
    $handler->display->display_options['defaults']['fields'] = FALSE;
    /* Field: Commerce Line Item: Line item ID */
    $handler->display->display_options['fields']['line_item_id']['id'] = 'line_item_id';
    $handler->display->display_options['fields']['line_item_id']['table'] = 'commerce_line_item';
    $handler->display->display_options['fields']['line_item_id']['field'] = 'line_item_id';
    $handler->display->display_options['fields']['line_item_id']['label'] = 'ID';
    $handler->display->display_options['fields']['line_item_id']['exclude'] = TRUE;
    /* Field: Commerce Line Item: Type */
    $handler->display->display_options['fields']['type']['id'] = 'type';
    $handler->display->display_options['fields']['type']['table'] = 'commerce_line_item';
    $handler->display->display_options['fields']['type']['field'] = 'type';
    $handler->display->display_options['fields']['type']['exclude'] = TRUE;
    /* Field: Commerce Line Item: Title */
    $handler->display->display_options['fields']['line_item_title']['id'] = 'line_item_title';
    $handler->display->display_options['fields']['line_item_title']['table'] = 'commerce_line_item';
    $handler->display->display_options['fields']['line_item_title']['field'] = 'line_item_title';
    /* Field: Commerce Line Item: Label */
    $handler->display->display_options['fields']['line_item_label']['id'] = 'line_item_label';
    $handler->display->display_options['fields']['line_item_label']['table'] = 'commerce_line_item';
    $handler->display->display_options['fields']['line_item_label']['field'] = 'line_item_label';
    $handler->display->display_options['fields']['line_item_label']['alter']['alter_text'] = TRUE;
    $handler->display->display_options['fields']['line_item_label']['alter']['text'] = '([line_item_label])';
    /* Field: Commerce Line item: Unit price */
    $handler->display->display_options['fields']['commerce_unit_price']['id'] = 'commerce_unit_price';
    $handler->display->display_options['fields']['commerce_unit_price']['table'] = 'field_data_commerce_unit_price';
    $handler->display->display_options['fields']['commerce_unit_price']['field'] = 'commerce_unit_price';
    $handler->display->display_options['fields']['commerce_unit_price']['click_sort_column'] = 'amount';
    $handler->display->display_options['fields']['commerce_unit_price']['type'] = 'commerce_price_formatted_amount';
    /* Field: Commerce Line Item: Quantity */
    $handler->display->display_options['fields']['quantity']['id'] = 'quantity';
    $handler->display->display_options['fields']['quantity']['table'] = 'commerce_line_item';
    $handler->display->display_options['fields']['quantity']['field'] = 'quantity';
    $handler->display->display_options['fields']['quantity']['precision'] = '0';
    /* Field: Commerce Line item: Total */
    $handler->display->display_options['fields']['commerce_total']['id'] = 'commerce_total';
    $handler->display->display_options['fields']['commerce_total']['table'] = 'field_data_commerce_total';
    $handler->display->display_options['fields']['commerce_total']['field'] = 'commerce_total';
    $handler->display->display_options['fields']['commerce_total']['click_sort_column'] = 'amount';
    $handler->display->display_options['fields']['commerce_total']['type'] = 'commerce_price_formatted_amount';
    /* Field: Commerce Line item: Priceminister transporter name */
    $handler->display->display_options['fields']['priceminister_transporter_name']['id'] = 'priceminister_transporter_name';
    $handler->display->display_options['fields']['priceminister_transporter_name']['table'] = 'field_data_priceminister_transporter_name';
    $handler->display->display_options['fields']['priceminister_transporter_name']['field'] = 'priceminister_transporter_name';
    $handler->display->display_options['fields']['priceminister_transporter_name']['label'] = 'Tracking informations';
    $handler->display->display_options['fields']['priceminister_transporter_name']['alter']['alter_text'] = TRUE;
    $handler->display->display_options['fields']['priceminister_transporter_name']['alter']['text'] = 'Transporter name : [priceminister_transporter_name]';
    /* Field: Commerce Line item: Priceminister tracking number */
    $handler->display->display_options['fields']['priceminister_tracking_number']['id'] = 'priceminister_tracking_number';
    $handler->display->display_options['fields']['priceminister_tracking_number']['table'] = 'field_data_priceminister_tracking_number';
    $handler->display->display_options['fields']['priceminister_tracking_number']['field'] = 'priceminister_tracking_number';
    $handler->display->display_options['fields']['priceminister_tracking_number']['alter']['alter_text'] = TRUE;
    $handler->display->display_options['fields']['priceminister_tracking_number']['alter']['text'] = 'Tracking number : [priceminister_tracking_number]';
    /* Field: Commerce Line item: Priceminister tracking url */
    $handler->display->display_options['fields']['priceminister_tracking_url']['id'] = 'priceminister_tracking_url';
    $handler->display->display_options['fields']['priceminister_tracking_url']['table'] = 'field_data_priceminister_tracking_url';
    $handler->display->display_options['fields']['priceminister_tracking_url']['field'] = 'priceminister_tracking_url';
    $handler->display->display_options['fields']['priceminister_tracking_url']['alter']['alter_text'] = TRUE;
    $handler->display->display_options['fields']['priceminister_tracking_url']['alter']['text'] = 'Tracing url : [priceminister_tracking_url]';
    /* Field: Commerce Line item: Status */
    $handler->display->display_options['fields']['priceminister_status']['id'] = 'priceminister_status';
    $handler->display->display_options['fields']['priceminister_status']['table'] = 'field_data_priceminister_status';
    $handler->display->display_options['fields']['priceminister_status']['field'] = 'priceminister_status';
    /* Field: Commerce Line Item: Priceminister line item actions */
    $handler->display->display_options['fields']['priceminister_actions']['id'] = 'priceminister_actions';
    $handler->display->display_options['fields']['priceminister_actions']['table'] = 'commerce_line_item';
    $handler->display->display_options['fields']['priceminister_actions']['field'] = 'priceminister_actions';
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Commerce Line Item: Order ID */
    $handler->display->display_options['arguments']['order_id']['id'] = 'order_id';
    $handler->display->display_options['arguments']['order_id']['table'] = 'commerce_line_item';
    $handler->display->display_options['arguments']['order_id']['field'] = 'order_id';
    $handler->display->display_options['arguments']['order_id']['default_action'] = 'empty';
    $handler->display->display_options['arguments']['order_id']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['order_id']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['order_id']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['order_id']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['defaults']['filter_groups'] = FALSE;
    $handler->display->display_options['defaults']['filters'] = FALSE;
    /* Filter criterion: Commerce Line Item: Line item is of a product line item type */
    $handler->display->display_options['filters']['product_line_item_type']['id'] = 'product_line_item_type';
    $handler->display->display_options['filters']['product_line_item_type']['table'] = 'commerce_line_item';
    $handler->display->display_options['filters']['product_line_item_type']['field'] = 'product_line_item_type';
    $handler->display->display_options['filters']['product_line_item_type']['value'] = '1';
  }
}
