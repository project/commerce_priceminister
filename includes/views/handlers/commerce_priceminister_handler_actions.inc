<?php

/**
 * @file
 * Field handler to present a button to remove a line item. It's a dummy
 * handler, most part of the implementation is done via pre and post render
 * hooks.
 */

/**
 * Field handler to present two buttons to accept/reject line item.
 */
class commerce_priceminister_handler_actions extends views_handler_field_entity {

  function construct() {
    parent::construct();
    $this->additional_fields['line_item_id'] = 'line_item_id';

    // Set real_field in order to make it generate a field_alias.
    $this->real_field = 'line_item_id';
  }

  function render($values) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    $line_item = $this->get_value($values);

    $status = $line_item->priceminister_status[LANGUAGE_NONE][0]['value'];
    $links = array();
    if ($status == 'to_confirm') {
      $links['accept'] = array(
        'title' => t('Accept'),
        'href' => 'admin/commerce/priceminister/' . $line_item->line_item_id . '/accept',
      );
      $links['refuse'] = array(
        'title' => t('Refuse'),
        'href' => 'admin/commerce/priceminister/' . $line_item->line_item_id . '/refuse',
      );
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
    elseif ($status == 'accepted' && empty($line_item->priceminister_transporter_name[LANGUAGE_NONE][0]['value']) || empty($line_item->priceminister_tracking_number[LANGUAGE_NONE][0]['value'])) {
      $links['tracking'] = array(
        'title' => t('Tracking'),
        'href' => 'admin/commerce/priceminister/' . $line_item->line_item_id . '/tracking',
      );
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
    else {
      return t('No more actions');
    }
  }
}
