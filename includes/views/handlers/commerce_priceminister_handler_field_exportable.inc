<?php

/**
 * @file
 * Field handler to display the product exportable status.
 */

/**
 * Field handler to display the product exportable status.
 */
class commerce_priceminister_handler_field_exportable extends views_handler_field_entity {

  function construct() {
    parent::construct();
    $this->additional_fields['product_id'] = 'product_id';

    $this->real_field = 'product_id';
  }

  function render($values) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    $product_id = $values->product_id;
    // TODO: find another way to get the feed id argument.
    $feed_id = $values->commerce_priceminister_feeds_commerce_product_id;
    $exportable = FALSE;
    if ($product_id && $feed_id) {
      $exportable = _commerce_priceminister_is_product_exportable($product_id, $feed_id);
    }

    if ($exportable) {
      $class = 'exportable';
      $value = t('YES');
    }
    else {
      $class = 'unexportable';
      $value = t('NO') . ' - ' . l(t('more details'), 'admin/commerce/config/marketplaces/priceminister/product/report/' . $values->commerce_product_sku . '/details');
    }
    return '<span class="' . $class . '" >' . $value . '</span>';
  }
}
