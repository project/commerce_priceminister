<?php

/**
 * Argument handler to display product titles in View using product arguments.
 */
class commerce_priceminister_handler_argument_feed_id extends views_handler_argument_numeric {
  function title_query() {
    $titles = array();
    $result = db_select('commerce_priceminister_feeds', 'cpf')
      ->fields('cpf', array('title'))
      ->condition('cpf.id', $this->value)
      ->execute();
    foreach ($result as $product) {
      $titles[] = check_plain($product->title);
    }
    return $titles;
  }
}
