<?php

/**
 * Implements hook_views_data()
 */
function commerce_priceminister_views_data() {
  $data = array();

  $data['commerce_priceminister_feeds']['table']['group']  = t('Commerce Priceminister Feed');

  $data['commerce_priceminister_feeds']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Commerce Priceminister feed'),
    'help' => t('Priceminister feed.'),
  );
  $data['commerce_priceminister_feeds']['table']['entity type'] = 'commerce_priceminister_feed';

  // Priceminister Feed ID field & argument handlers.
  $data['commerce_priceminister_feeds']['id'] = array(
    'title' => t('Priceminister feed id'),
    'field' => array(
      'title' => t('Priceminister feed id'),
      'help' => t('Display Priceminister feed id'),
    ),
    'argument' => array(
      'title' => t('Priceminister feed id'),
      'help' => t('Use Priceminister feed id as argument.'),
      'handler' => 'commerce_priceminister_handler_argument_feed_id',
    ),
  );
  // Priceminister feed relationship to products.
  $data['commerce_priceminister_feeds']['local_product_type'] = array(
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_product',
      'field' => 'type',
      'label' => t('Priceminister feed -> products'),
      'help' => t('Relationship from one Priceminister feed to many produdcts.'),
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter()
 */
function commerce_priceminister_views_data_alter(&$data) {

  // Priceminiser line item actions (accept/reject).
  $data['commerce_line_item']['priceminister_actions'] = array(
    'field' => array(
      'title' => t('Priceminister line item actions'),
      'help' => t('Adds actions to accept/reject a line item.'),
      'handler' => 'commerce_priceminister_handler_actions',
    ),
  );

  // Priceminiser product exportable state.
  $data['commerce_product']['priceminister_exportable'] = array(
    'field' => array(
      'title' => t('Priceminister product is exportable'),
      'help' => t('Display if a product is exportable or not.'),
      'handler' => 'commerce_priceminister_handler_field_exportable',
      'click sortable' => TRUE,
    ),
  );

  // Priceminister feed relationship from a product.
  $data['commerce_product']['priceminister_type'] = array(
    'title' => t('Priceminister feed <- product'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_priceminister_feeds',
      'base field' => 'local_product_type',
      'field' => 'type',
      'label' => t('Product -> Priceminister feed'),
      'help' => t('Relationship from one product to one Priceminister feed.'),
    ),
  );
  $data['views_entity_commerce_order']['operations_dropbutton_priceminister'] = array(
    'field' => array(
      'title' => t('Operations links (Dropbutton) - Priceminister'),
      'help' => t('Display the available operations links for the order in a dropbutton.'),
      'handler' => 'commerce_priceminister_order_handler_field_order_operations',
    ),
  );

  return $data;
}