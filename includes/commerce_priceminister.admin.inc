<?php

/**
 * Configuration form to store Priceminister settings.
 */
function commerce_priceminister_configure_form($form, &$form_state) {
  $commerce_priceminister_api = variable_get('commerce_priceminister_api', array());

  $form['commerce_priceminister_api'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => 'API Credentials',
  );
  // Login.
  $form['commerce_priceminister_api']['login'] = array(
    '#type' => 'textfield',
    '#title' => t('Login'),
    '#default_value' => isset($commerce_priceminister_api['login']) ? $commerce_priceminister_api['login'] : '',
    '#required' => TRUE,
  );
  // Password.
  $form['commerce_priceminister_api']['pwd'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => isset($commerce_priceminister_api['pwd']) ? $commerce_priceminister_api['pwd'] : '',
    '#required' => TRUE,
  );
  $form['commerce_priceminister_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Mode'),
    '#options' => array(
      'test' => ('Test - allow you to test in real conditions on Priceminister sandboxes'),
      'live' => ('Live - process real export import to a live account'),
    ),
    '#default_value' => variable_get('commerce_priceminister_mode', 'test'),
  );
  $form = system_settings_form($form);
  $form['#submit'][] = 'commerce_priceminister_configure_form_submit';
  return $form;
}

/**
 * Validate API credentials.
 */
function commerce_priceminister_configure_form_submit($form, &$form_state) {
  $api = new PriceministerAPI();
  $result = $api->getProductTypes();
  if ($result) {
    drupal_set_message(t('API credentials successfully validated'));
  }
  else {
    drupal_set_message(t('API credentials dosen\'t seems to be valid'), 'warning');
  }
}
