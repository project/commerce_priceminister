<?php
/**
 * @file
 *
 * Import orders from Priceminister.
 */

/**
 * Manually import orders from Priceminister.
 */
function commerce_priceminister_import_orders_form($form, &$form_state) {
  $question = t('Import orders');
  $description = t('Your are going to import orders from priceminister.');
  $path = 'admin/commerce/config/marketplaces/priceminister/orders';
  return confirm_form($form, $question, $path, $description);
}

function commerce_priceminister_import_orders_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/commerce/config/marketplaces/priceminister/orders';
  $results = commerce_priceminister_import_orders();
  commerce_priceminister_import_update_line_item();
  drupal_set_message(format_string('Imported orders : %count', array('%count' => count($results))));
}

/**
 * Fetch all new order from Priceminister
 * and store them as priceminister order bundle.
 */
function commerce_priceminister_import_orders() {
  $imported_orders = array();
  $api = new PriceministerAPI();

  // Import new orders.
  $priceminister_orders_new = $api->getOrders();
  if (!empty($priceminister_orders_new)) {
    $imported_orders = array_merge($imported_orders, _commerce_priceminister_import_orders($priceminister_orders_new, 'pending'));
  }

  // Import current orders.
  $priceminister_orders_current = $api->getOrders(FALSE);
  if (!empty($priceminister_orders_current)) {
    $imported_orders = array_merge($imported_orders, _commerce_priceminister_import_orders($priceminister_orders_current, 'processing'));
  }

  return $imported_orders;
}

function _commerce_priceminister_import_orders($priceminister_orders, $order_status) {
  include_once DRUPAL_ROOT . '/includes/locale.inc';

  $imported_orders = array();
  $countries = country_get_list();
  $countries = array_flip($countries);
  // Remove already imported orders, based on Priceminister remote id.
  $query = new EntityFieldQuery();
  $results = $query->entityCondition('entity_type', 'commerce_order')
    ->entityCondition('bundle', 'priceminister')
    ->fieldCondition('priceminister_id', 'value', array_keys($priceminister_orders))
    ->execute();
  if (!empty($results['commerce_order'])) {
    $commerce_orders = $results['commerce_order'];
    $fields = field_info_instances('commerce_order', 'priceminister');
    $field_id = $fields['priceminister_id']['field_id'];
    field_attach_load('commerce_order', $commerce_orders, FIELD_LOAD_CURRENT, array('field_id' => $field_id));
    $already_imported_orders = array();
    foreach ($commerce_orders as $commerce_order) {
      $order_id = $commerce_order->order_id;
      $priceminister_id = (isset($commerce_order->priceminister_id[LANGUAGE_NONE]) && !empty($commerce_order->priceminister_id[LANGUAGE_NONE][0]['value'])) ? $commerce_order->priceminister_id[LANGUAGE_NONE][0]['value'] : NULL;
      if ($priceminister_id) {
        $priceminister_orders[$priceminister_id]['order_id'] = $order_id;
        $already_imported_orders[$priceminister_id] = $priceminister_id;
      }
    }
  }
  foreach ($priceminister_orders as $priceminister_order) {
    $commerce_order = _commerce_priceminister_import_order($priceminister_order, $order_status, $countries);
    $imported_orders[$commerce_order->order_id] = $commerce_order->order_id;
  }
  return $imported_orders;
}

function _commerce_priceminister_import_order($priceminister_order, $order_status, $countries) {
  $commerce_order = empty($priceminister_order['order_id']) ? commerce_order_new(0, $order_status, 'priceminister') : commerce_order_load($priceminister_order['order_id']);
  $commerce_order_wrapper = entity_metadata_wrapper('commerce_order', $commerce_order);
  $currencies = commerce_currency_get_code();
  if (empty($priceminister_order['order_id'])) {
    // Create order.
    $billing_profile = commerce_customer_profile_new('billing');
  }
  else {
    // Load order.
    $billing_profile = $commerce_order_wrapper->commerce_customer_billing->value();
  }
  $billing_profile_wrapper = entity_metadata_wrapper('commerce_customer_profile', $billing_profile);

  // Create billing profile.
  $address = array(
    'country' => $countries[$priceminister_order['shipping']['billing_address']['country']],
    'administrative_area' => '',
    'sub_administrative_area' => '',
    'locality' => $priceminister_order['shipping']['billing_address']['city'],
    'dependent_locality' => '',
    'postal_code' => $priceminister_order['shipping']['billing_address']['zipcode'],
    'thoroughfare' => $priceminister_order['shipping']['billing_address']['address1'],
    'premise' => $priceminister_order['shipping']['billing_address']['address2'],
    'sub_premise' => '',
    'organisation_name' => '',
    'name_line' => $priceminister_order['shipping']['billing_address']['firstname'] . ' ' . $priceminister_order['shipping']['billing_address']['lastname'],
    'first_name' => $priceminister_order['shipping']['billing_address']['firstname'],
    'last_name' => $priceminister_order['shipping']['billing_address']['lastname'],
  );

  $billing_profile_wrapper->commerce_customer_address = $address;
  $billing_profile_wrapper->save();

  // Set created date to Priceminister purchase date.
  $commerce_order_wrapper->created = DateTime::createFromFormat('d/m/Y-H:i', $priceminister_order['date'])->format('U');
  $commerce_order_wrapper->hostname = ip_address();
  $commerce_order_wrapper->priceminister_id = $priceminister_order['id'];
  $commerce_order_wrapper->priceminister_username = $priceminister_order['shipping']['customer'];
  $commerce_order_wrapper->commerce_customer_billing = $billing_profile;
  $commerce_order_wrapper->save();

  // Create line items.
  $line_items = $commerce_order_wrapper->commerce_line_items;
  if (!empty($line_items)) {
    foreach ($line_items as $line_item) {
      $line_item_id = $line_item->line_item_id->value();
      $priceminister_id = $line_item->priceminister_id->value();
      $priceminister_order['items'][$priceminister_id]['line_item_id'] = $line_item_id;
    }
  }
  foreach ($priceminister_order['items'] as $item) {
    if (!empty($item['line_item_id'])) {
      $line_item = commerce_line_item_load($item['line_item_id']);
    }
    else {
      $line_item = commerce_line_item_new('priceminister', $commerce_order->order_id);
    }
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    $line_item_wrapper->line_item_label = $item['title'];
    $line_item_wrapper->priceminister_id = $item['id'];
    $item['status'] = drupal_strtolower($item['status']);
    // Only decrement if the line item has been accepted.
    if ($line_item_wrapper->priceminister_status->value() != $item['status'] && $item['status'] == 'accepted') {
      $decrement = TRUE;
    }
    else {
      $decrement = FALSE;
    }
    $line_item_wrapper->priceminister_status = $item['status'];
    $line_item_wrapper->quantity = 1;

    $currency_code = $currencies[$item['price']['currency']];
    $amount = commerce_currency_decimal_to_amount($item['price']['amount'], $currency_code);
    $component = array(
      'amount' => $amount,
      'currency_code' => $currency_code,
      'data' => array(),
    );

    $line_item_wrapper->commerce_unit_price->amount = $amount;
    $line_item_wrapper->commerce_unit_price->currency_code = $currency_code;

    $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
      $line_item_wrapper->commerce_unit_price->value(),
      'base_price',
      $component,
      TRUE
    );

    if ($product = commerce_product_load_by_sku($item['sku'])) {
      $line_item_wrapper->commerce_product = $product;
    }
    if ($decrement) {
      _commerce_priceminister_decrement_stock($line_item_wrapper);
    }
    $line_item_wrapper->save();
    if (empty($item['line_item_id'])) {
      $commerce_order_wrapper->commerce_line_items[] = $line_item;
    }
  }
  $commerce_order_wrapper->save();

  return $commerce_order;
}

function commerce_priceminister_import_update_line_item() {
  $api = new PriceministerAPI();
  $order_ids = commerce_priceminister_order_ids();
  foreach ($order_ids as $order_id) {
    $order = commerce_order_load($order_id);
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    foreach ($order_wrapper->commerce_line_items as $line_item)  {
      if ($line_item->priceminister_status->value() != 'accepted') {
        continue 2;
      }
    }
    $order_remote_id = $order_wrapper->priceminister_id->value();
    $line_items_informations = $api->getLineItemBillingInformation($order_remote_id);
    foreach ($line_items_informations as $line_item_remote_id => $line_items_information) {
      $found = FALSE;
      foreach ($order_wrapper->commerce_line_items as $line_item)  {
        if ($line_item->priceminister_id->value() == $line_item_remote_id) {
          $found = TRUE;
          break;
        }
      }
      if ($found) {
        if (!commerce_priceminister_price_component_exist($line_item, 'commerce_unit_price', 'base_price', $line_items_information['item_price'])) {
          $component = array(
            'amount' => $line_items_information['item_price']['amount'],
            'currency_code' => $line_items_information['item_price']['currency_code'],
            'data' => array(),
          );
          $line_item->commerce_unit_price->data = commerce_price_component_add(
            $line_item->commerce_unit_price->value(),
            'base_price',
            $component,
            TRUE
          );
        }
        $components_names = array('item_excl_tax','item_vat','shipping_price','shipping_excl_tax','shipping_vat');
        foreach ($components_names as $components_name) {
          if (isset($line_items_information[$components_name]['amount']) && isset($line_items_information[$components_name]['currency_code'])) {
            if (!commerce_priceminister_price_component_exist($line_item, 'commerce_unit_price', $components_name, $line_items_information[$components_name])) {
              $component = array(
                'amount' => $line_items_information[$components_name]['amount'],
                'currency_code' => $line_items_information[$components_name]['currency_code'],
                'data' => array(),
              );
              $line_item->commerce_unit_price->data = commerce_price_component_add(
                $line_item->commerce_unit_price->value(),
                $components_name,
                $component,
                TRUE
              );
            }
          }
        }

        $component_total = commerce_price_component_total($line_item->commerce_unit_price->value());

        // Add the totals.
        $amount = commerce_currency_convert(
          $component_total['amount'],
          $component_total['currency_code'],
          $line_items_information['item_price']['currency_code']
        );

        // Update the order total price field with the final total amount.
        $line_item->commerce_unit_price->amount = round($amount);
        $line_item->save();
      }
    }
  }
}
