<?php

/**
 * @file
 * Provides mechanism to communicate with Priceminister API.
 */

/**
 * Implements methods from Priceminister API.
 */
class PriceministerAPI {

  private $url;
  private $settings;

  /**
   * Define needed settings.
   */
  public function __construct() {
    // Set credentials.
    $settings = variable_get('commerce_priceminister_api', array());
    if (empty($settings)) {
      drupal_set_message(t('The module is not configured for used. Go to the !page to add your credentials.', array('!page' => l('configuration page', 'admin/commerce/config/marketplaces/priceminister/configure'))), 'error');
    }
    $this->setSettings($settings + array('from' => 'drupalcommerce'));

    // Define if we are in a sandbox or not.
    if (variable_get('commerce_priceminister_mode', 'test') == 'test') {
      $this->setUrl('https://ws.sandbox.priceminister.com');
    }
    else {
      $this->setUrl('https://ws.priceminister.com');
    }
  }

  /**
   * Implements producttypes method.
   *
   * Get Priceminister product types.
   *
   * @return array
   *   Array keyed by product type machine name.
   */
  public function getProductTypes() {
    $query = array(
      'action' => 'producttypes',
      'version' => $this->getVersion('producttypes'),
    ) + $this->getSettings();
    $xml = $this->doCall('stock_ws', $query, array('method' => 'GET'));

    $product_types = array();
    if ($xml) {
      foreach ($xml->response->producttypetemplate as $product_type) {
        $product_types[(string) $product_type->alias] = (string) $product_type->label;
      }
    }
    return $product_types;
  }

  /**
   * Implements producttypetemplate method.
   *
   * Get Priceminister product type fields informations.
   *
   * @param string $product_type
   *   Product type machine name from getProductTypes() method.
   *
   * @return array
   *   Array keyed by field name containing field infos.
   */
  public function getProductTypeFields($product_type) {
    $query = array(
      'action' => 'producttypetemplate',
      'alias' => $product_type,
      'scope' => 'VALUES',
      'version' => $this->getVersion('producttypetemplate'),
    ) + $this->getSettings();
    $xml = $this->doCall('stock_ws', $query, array('method' => 'GET'));

    $product_type_fields = array();
    if ($xml) {
      foreach ($xml->response->attributes->children() as $categorie) {
        $parent = $categorie->getName();
        foreach ($categorie->children() as $value) {
          $key = (string) $value->key;
          if ($key) {
            $product_type_fields[$key] = array(
              'label' => (string) $value->label,
              'key' => $key,
              'mandatory' => (int) $value->mandatory,
              'valuetype' => (string) $value->valuetype,
              'multivalued' => (int) $value->multivalued,
              'hasvalues' => (int) $value->hasvalues,
              'parent' => $parent,
            );
            if ($value->valueslist) {
              foreach ($value->valueslist->children() as $valuelist) {
                $product_type_fields[$key]['valueslist'][] = (string) $valuelist;
              }
              $product_type_fields[$key]['valueslist'] = drupal_map_assoc($product_type_fields[$key]['valueslist']);
            }
            if ($key == 'state') {
              /**
               * Special case for state field
               * See 2.2.6. Condition attribute(code : <state>)
               * https://developer.priceminister.com/blog/en/documentation/inventory-management/import-xml/xml-structure
               */
              $product_type_fields[$key]['valuetype'] = "Number";
              $product_type_fields[$key]['hasvalues'] = "1";
              $product_type_fields[$key]['valueslist'] = drupal_map_assoc(array(
                0  => 'New',
                10 => 'As new',
                20 => 'Very good',
                30 => 'Good',
                40 => 'Acceptable',
                50 => 'Out of order',
              ));
            }
          }
        }
      }
    }
    return $product_type_fields;
  }

  /**
   * Implements genericimportfile method.
   *
   * Export items (products, advert, media, etc ...) to Priceminister store.
   * Items are exported within an XML file which is stored in Priceminister
   * queue for later processing.
   *
   * @param array $items
   *   Items generated with generateItem().
   *
   * @return array
   *   Array containing file_id, status and item_ids keys.
   */
  public function exportProducts(array $items = array()) {
    $query = array(
      'action' => 'genericimportfile',
    );
    $files = array(
      'file' => array(
        'name' => 'xml',
        'content' => $this->generateXML($items),
      )
    );
    $boundary = md5(time());

    // Post generated XML file using multipart/form-data header.
    $options = array(
      'method' => 'POST',
      'headers' => array('Content-Type' => 'multipart/form-data; boundary=' . $boundary),
      'data' => $this->generateMultipartFormData($boundary, array('version' => $this->getVersion('genericimportfile')) + $this->getSettings(), $files),
    );
    $xml = $this->doCall('stock_ws', $query, $options);

    $file = array();
    $file['product_skus'] = array_keys($items);
    if ($xml) {
      /*
       * The file is remotely queued at Priceminister.
       * We need to store the file id to be able to get an export report later.
       */
      $file['file_id'] = (string) $xml->response->importid;
      $file['status'] = (string) $xml->response->status;
    }
    return $file;
  }

  /**
   * Implements genericimportreport method.
   *
   * Get import report informations.
   * Get informations about queued file in Priceminister queue.
   *
   * @param string $file_id
   *   File id from genericimportfile method.
   *
   * @return array
   *   Array containing status and product_skus keys.
   */

  public function getExportReport($feed_id, $item, $token = NULL) {
    $file_id = $item->data['file_id'];
    $query = array(
      'action' => 'genericimportreport',
      'fileid' => $file_id,
      'nexttoken' => $token,
      'version' => $this->getVersion('genericimportreport'),
    ) + $this->getSettings();

    $xml = $this->doCall('stock_ws', $query, array('method' => 'GET'));
    $return = FALSE;
    if (!$xml) {
      return array(
        'return' => $return,
        'report' => array(),
        'errors' => array(),
      );
    }

    $product_errors = array();
    $success_rate = (string) $xml->response->file->successrate;
    $uploaded = date_create_from_format('Y-m-d\TH:i:s', (string) $xml->response->file->uploaddate);
    $processed = date_create_from_format('Y-m-d\TH:i:s', (string) $xml->response->file->processdate);
    $report_data = array(
      'feed_id'           => $feed_id,
      'remote_id'         => $file_id,
      'status'            => FALSE,
      'description'       => NULL,
      'success_rate'      => empty($success_rate) ? "0%" : $success_rate,
      'total_products'    => (int) $xml->response->file->totallines,
      'exported_products' => (int) $xml->response->file->processedlines,
      'error_products'    => (int) $xml->response->file->errorlines,
      'uploaded'          => $uploaded ? $uploaded->getTimestamp() : NULL,
      'processed'         => $processed ? $processed->getTimestamp() : NULL,
    );

    switch ($xml->response->file->status) {
      case 'Traité':
        $return = TRUE;
        if ($report_data['total_products'] == $report_data['exported_products'] && $report_data['error_products'] == 0) {

          /**
           * Success: every items in the file has been exported.
           * - TODO: update metadata with remote id
           * - remove file from queue
           */
          $report_data['status'] = 'Success';
          $report_data['description'] = t('Every products have been exported.');
          $report_data['exported_products'] = count($item->data['product_skus']);

        }
        else {
          /**
           * Warning : error happened on products.
           * - notity faulty products
           * - remove file from queue
           */
          $report_data['status'] = t('Warning');
          $success_lines = $report_data['total_products'] - $report_data['error_products'];
          $report_data['description'] = format_plural($success_lines, t('1 product has been exported.'), t('@count products have been exported.'));
          foreach ($xml->response->product as $product) {
            // Populate with error product ids.
            $sku = (string) $product->sku;
            $product_errors[$sku] = array(
              'sku' => $sku,
              'errors' => array(),
            );
            foreach ($product->errors->children() as $error) {
              $error_field = (string) $error->error_key;
              $error_code = (string) $error->error_code;
              $error_message = (string) $error->error_text;
              $product_errors[$sku]['errors'][] = array(
                'message' => $error_message,
                'code'    => $error_code,
                'field'   => $error_field,
              );
            }
          }
        }
        break;

      case 'Reçu':
      case 'En attente':
        $return = FALSE;
        $report_data['status'] = t('Pending');
        $report_data['description'] = t('Products are waiting to be processed by Priceminister.');
        break;

      case 'M.à.j. en cours':
        /**
         * Pending : file is remotely pending.
         * - release file from queue to re-process it later.
         */
        $return = FALSE;
        $report_data['status'] = t('Processing');
        $report_data['description'] = t('Products are being processed by Priceminister.');
        break;

      default:
        //case 'Annulé':
        //case 'Aucune ligne n’a été chargée':
        /**
         * Fail : file has not been exported.
         * - re-queue products
         * - remove file from queue
         */
        $return = TRUE;
        $report_data['status'] = t('Canceled');
        $report_data['description'] = t('Products have not been exported');
        $products_queue = DrupalQueue::get('priceminister_product_feed_' . $feed_id);
        foreach ($item->data['product_skus'] as $product_sku) {
          $product = commerce_product_load_by_sku($product_sku);
          if (_commerce_priceminister_is_product_exportable($product->product_id, $feed_id)) {
            $products_queue->createItem($product->sku);
          }
        }
        break;
    }
    // TODO: recursive call if nextToken is defined. see https://developer.priceminister.com/blog?qa_faqs=comment-bien-utiliser-la-variable-nexttoken
    $nexttoken = (string) $xml->response->nexttoken;
    if (!empty($nexttoken)) {
      $nextdata = $this->getExportReport($feed_id, $item, $nexttoken);
      $product_errors = array_merge_recursive($product_errors, $nextdata['errors']);
    }

    return array(
      'return' => $return,
      'report' => $report_data,
      'errors' => $product_errors,
    );
  }

  /**
   * Implements getnewsales method.
   *
   * Fetch Priceminister orders.
   *
   * @param bool $new_orders
   *   TRUE to fetch new orders : orders to be confirmed.
   *   FALSE to fetch already : already confirmed orders.
   * @param string $nexttoken (optional)
   *   An id used to fetch additional data in case of pagination.
   *
   * @return array $orders
   *   Array containing fetched orders.
   */
  public function getOrders($new_orders = TRUE, $nexttoken = NULL) {
    if ($new_orders) {
      $query = array(
        'action' => 'getnewsales',
        'version' => $this->getVersion('getnewsales'),
      ) + $this->getSettings();
    }
    else {
      $query = array(
        'action' => 'getcurrentsales',
        'version' => $this->getVersion('getcurrentsales'),
        'nexttoken' => $nexttoken,
      ) + $this->getSettings();
      // TODO: recursive call if nextToken is defined. see https://developer.priceminister.com/blog?qa_faqs=comment-bien-utiliser-la-variable-nexttoken
    }
    $xml = $this->doCall('sales_ws', $query, array('method' => 'GET'));

    $orders = array();
    if ($xml) {
      foreach ($xml->response->sales->children() as $order) {
        /**
         * TODO: handle errors, check for undefined data.
         * Test different shipping services.
         */
        $order_number = (string) $order->purchaseid;
        if ($order_number) {
          if (in_array((string) $order->deliveryinformation->shippingtype, array('So Colissimo', 'Chronopost', 'Retrait chez le vendeur'))) {
            $address_key = 'billingaddress';
          }
          else {
            $address_key = 'deliveryaddress';
          }
          $orders[$order_number] = array(
            'id' => $order_number,
            'date' => (string) $order->purchasedate,
            'shipping' => array(
              'type' => (string) $order->deliveryinformation->shippingtype,
              'price' => array(
                'amount' => (string) $order->deliveryinformation->purchaseshippingcostprice->amount,
                'currency' => (string) $order->deliveryinformation->purchaseshippingcostprice->currency,
              ),
              'customer' => (string) $order->deliveryinformation->purchasebuyerlogin,
              'billing_address' => array(
                'civility' => (string) $order->deliveryinformation->{$address_key}->civility,
                'lastname' => (string) $order->deliveryinformation->{$address_key}->lastname,
                'firstname' => (string) $order->deliveryinformation->{$address_key}->firstname,
                'address1' => (string) $order->deliveryinformation->{$address_key}->address1,
                'address2' => (string) $order->deliveryinformation->{$address_key}->address2,
                'zipcode' => (string) $order->deliveryinformation->{$address_key}->zipcode,
                'city' => (string) $order->deliveryinformation->{$address_key}->city,
                'country' => (string) $order->deliveryinformation->{$address_key}->country,
                'countryalpha2' => (string) $order->deliveryinformation->{$address_key}->countryalpha2,
              ),
            ),
            'items' => array(),
          );
          foreach ($order->items->children() as $item) {
            $orders[$order_number]['items'][(string) $item->itemid] =  array(
              'id' => (string) $item->itemid,
              'sku' => (string) $item->sku,
              'isbn' => (string) $item->isbn,
              'ean' => (string) $item->ean,
              'title' => (string) $item->headline,
              'sellerscore' => (string) $item->sellerscore,
              'status' => (string) $item->itemstatus,
              'ispreorder' => (string) $item->ispreorder,
              'isnego' => (string) $item->isnego,
              'price' => array(
                'amount' => (string) $item->price->amount,
                'currency' => (string) $item->price->currency,
              ),
              'advertpricelisted' => array(
                'amount' => (string) $item->advertpricelisted->amount,
                'currency' => (string) $item->advertpricelisted->currency,
              ),
              'itemshippingcostprice' => array(
                'amount' => (string) $item->itemshippingcostprice->amount,
                'currency' => (string) $item->itemshippingcostprice->currency,
              ),
            );
          }
        }
      }
    }
    return $orders;
  }

  /**
   * Implements action method.
   *
   * Set a status to a line item.
   * Status can be either accepted or refused.
   *
   * @param string $status
   *   Possible values : acceptsale OR refusesale
   * @param string $line_item_remote_id
   *   Line item remote id
   *
   * @return string
   *   Line item new status
   */
  public function setLineItemStatus($line_item_remote_id, $status) {
    $query = array(
        'action' => $status,
        'version' => $this->getVersion($status),
        'itemid' => $line_item_remote_id,
      ) + $this->getSettings();
    $xml = $this->doCall('sales_ws', $query, array('method' => 'GET'));

    $return_status = NULL;
    if ($xml) {
      if (isset($xml->response->status)) {
        $return_status = (string) $xml->response->status;
      }
      elseif (isset($xml->error->details->detail)) {
        $detail = (string) $xml->error->details->detail;
        if ($detail == 'Current status : CAPTURED - Required status : REQUESTED') {
          // Already accepted
          $return_status = 'accepted';
        }
        elseif ($detail == 'Current status : EMPTIED – Required status : REQUESTED') {
          // Already refused
          $return_status = 'refused';
        }
      }
    }
    return $return_status;
  }

  /**
   * Implements settrackingpackageinfos method.
   *
   * Set tracking information for a line item.
   *
   * @param string $line_item_remote_id
   *   Line item remote id
   * @param string $transporter_name
   *   Name of the transporter
   * @param string $tracking_number
   *   Tracking code
   * @param string $tracking_url (optional)
   *   Url used to track the package.
   *
   * @return string
   *   Return OK in case of success.
   */
  public function setLineItemTrackingInformations($line_item_remote_id, $transporter_name, $tracking_number, $tracking_url = NULL) {
    $query = array(
        'action' => 'settrackingpackageinfos',
        'version' => $this->getVersion('settrackingpackageinfos'),
        'itemid' => $line_item_remote_id,
        'transporter_name' => $transporter_name,
        'tracking_number' => $tracking_number,
        'tracking_url' => $tracking_url,
      ) + $this->getSettings();
    $xml = $this->doCall('sales_ws', $query, array('method' => 'GET'));

    $status = NULL;
    if ($xml) {
      if (isset($xml->response->status)) {
        $status = (string) $xml->response->status;
      }
    }
    return $status;
  }

  public function getLineItemBillingInformation($order_remote_id) {
    $query = array(
      'action' => 'getbillinginformation',
      'version' => $this->getVersion('getbillinginformation'),
      'purchaseid' => $order_remote_id,
    ) + $this->getSettings();
    $xml = $this->doCall('sales_ws', $query, array('method' => 'GET'));

    $line_item_billing_information = array();
    if ($xml) {
      foreach ($xml->response->billinginformation->items->children() as $item) {
        $line_item_remote_id = (string) $item->itemid;
        $line_item_billing_information[$line_item_remote_id] = array(
          'remote_id' => $line_item_remote_id,
          'sku' => (string) $item->sku,
          'title' => (string) $item->headline,
          'category' => (string) $item->category,
          'status' => (string) $item->status,
          'item_vat_rate' => (string) $item->itemcommissionvatrate,
          'shipping_vat_rate' => (string) $item->shippingcommissionvatrate,
          'item_price' => array(
            'amount' => (float) $item->itemsaleprice-> amount * 100,
            'currency_code' => (string) $item->itemsaleprice->currency,
          ),
          'shipping_price' => array(
            'amount' => (float) $item->shippingsaleprice-> amount * 100,
            'currency_code' => (string) $item->shippingsaleprice->currency,
          ),
          'item_excl_tax' => array(
            'amount' => (float) $item->itemcommissionexcltax-> amount * -100,
            'currency_code' => (string) $item->itemcommissionexcltax->currency,
          ),
          'item_vat' => array(
            'amount' => (float) $item->itemcommissionvat-> amount * -100,
            'currency_code' => (string) $item->itemcommissionvat->currency,
          ),
          'shipping_excl_tax' => array(
            'amount' => (float) $item->shippingcommissionexcltax-> amount * -100,
            'currency_code' => (string) $item->shippingcommissionexcltax->currency,
          ),
          'shipping_vat' => array(
            'amount' => (float) $item->shippingcommissionvat->amount * -100,
            'currency_code' => (string) $item->shippingcommissionvat->currency,
          ),
          'payment_amount' => array(
            'amount' => (float) $item->paymentamount->amount * 100,
            'currency_code' => (string) $item->paymentamount->currency,
          ),
        );
      }
    }
    return $line_item_billing_information;
  }

  /**
   * Process call to the Priceminister API.
   *
   * Wrapper around drupal_http_request() to process API call and handle errors.
   *
   * @param array $query
   *   Query array to pass to url().
   * @param $options
   *   Options array to pass to drupal_http_request().
   *
   * @return bool|SimpleXMLElement
   *   success : SimpleXMLElement object
   *   error : FALSE
   */
  private function doCall($service, $query, $options) {
    $query['channel'] = 'commerceguys';
    $url = url($this->getUrl($service), array(
      'query' => $query,
    ));
    $result = drupal_http_request($url, $options + array('timeout' => 15));

    $return = FALSE;
    if (property_exists($result, 'error')) {
      // Http request error
      $error['code'] = $result->code;
      $error['message'] = $result->error;
    }
    elseif (!empty($result->data)) {
      try {
        $xml = new SimpleXMLElement($result->data, LIBXML_NOCDATA);
        if (empty($xml->error)) {
          $return = $xml;
        }
        else {
          // Special case for sale_ws which return needed error status.
          if ($service == 'sales_ws' && in_array($query['action'], array('acceptsale', 'refusesale'))) {
            $return = $xml;
          }
          // API error
          $error['code'] = (string) $xml->error->code;
          $error['message'] = (string) $xml->error->details->detail;
        }
      }
      catch (Exception $e) {
        // SimpleXML error
        $error['code'] = $e->getCode();
        $error['message'] = $e->getMessage();
      }
    }
    if (!empty($error)) {
      // Log API error.
      watchdog('commerce_priceminister', 'PriceministerAPI @code: @error', array('@code' => $error['code'], '@error' => $error['message']), WATCHDOG_ERROR);
      $return = FALSE;
    }

    return $return;
  }

  /**
   * Helper to generate the multipart/form-data request.
   *
   * @param $boundary
   *   A random string used as header boundaries.
   * @param array $postdata
   *   Array containing regular arguments.
   * @param array $files
   *   Array containing files to be posted.
   *
   * @return string
   *   Encoded multipart/form-data header.
   */
  private function generateMultipartFormData($boundary, $postdata = array(), $files = array()) {
    $data = '';
    $eol = "\r\n";

    foreach ($postdata as $name => $value) {
      $data .= '--' . $boundary . $eol;
      $data .= 'Content-Disposition: form-data; name="' . $name . '"' . $eol . $eol;
      $data .= $value . $eol;
    }

    foreach ($files as $name => $file) {
      $data .= '--' . $boundary . $eol;
      $data .= 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $file['name'] . '"' . $eol;
      $data .= 'Content-Type: application/octet-stream' . $eol . $eol;
      $data .= $file['content'] . $eol;
    }
    $data .= '--' . $boundary . '--' . $eol . $eol;

    return $data;
  }

  /**
   * Helper to generate the needed XML file for genericimportfile method.
   *
   * @param array $items
   *   Array of items used to populate the file with.
   *
   * @return mixed
   *   Plain XML.
   */
  public function generateXML($items = array()) {
    $xml = new XMLWriter();
    $xml->openMemory();
    $xml->startDocument('1.0', 'UTF-8');
    $xml->startElement('items');
    $this->arrayToXML($items, $xml, TRUE);
    $xml->endElement();
    $xml->endDocument();
    return $xml->outputMemory();
  }

  /**
   * Helper to convert items array to XML.
   *
   * @param array $array
   *   Items to be converted.
   * @param XMLWriter $xml
   *   The XMLWriter object to work on.
   */
  private function arrayToXML(array $array, XMLWriter $xml, $root = FALSE) {
    foreach ($array as $k => $v) {
      if (is_array($v)) {
        if ($root) {
          $k = 'item';
        }
        $xml->startElement($k);
        $this->arrayToXML($v, $xml);
        $xml->endElement();
      }
      else {
        if ($k != 'alias') {
          $xml->startElement('attribute');
          $xml->writeElement('key', $k);
          $xml->startElement('value');
          if (is_string($v)) {
            $xml->writeCdata($v);
          }
          else {
            $xml->writeRaw($v);
          }
          $xml->endElement();
          $xml->endElement();
          //$attribute->addChild('unit', ??);
        }
        else {
          $xml->writeElement($k, $v);
        }
      }
    }
  }

  /**
   * Get version parameter to use for a specific action.
   *
   * Priceminister provide a list of versions available for each actions.
   * see https://developer.priceminister.com/blog/en/documentation/pdf#version
   *
   * @param string $action
   *   Action parameter sent to Priceminister API.
   *
   * @return string
   *   Version string to use.
   */
  private function getVersion($action) {
    $versions = array(
      'producttypes'            => '2011-11-29',
      'producttypetemplate'     => '2011-11-29',//'2013-05-14',
      'genericimportfile'       => '2011-11-29',//'2012-09-11',
      'genericimportreport'     => '2011-11-29',
      'getnewsales'             => '2013-01-07',//'2013-06-25',
      'acceptsale'              => '2010-09-20',
      'refusesale'              => '2010-09-20',
      'getcurrentsales'         => '2013-04-25',//'2013-06-25',
      'settrackingpackageinfos' => '2012-11-06',
      'getbillinginformation'   => '2011-03-29',
    );
    return isset($versions[$action]) ? $versions[$action] : NULL;
  }

  private function setSettings($settings) {
    $this->settings = $settings;
  }

  private function getSettings() {
    return $this->settings;
  }

  private function setUrl($url) {
    $this->url = $url;
  }

  private function getUrl($service = NULL) {
    return rtrim($this->url, '/') . '/' . $service;
  }
}