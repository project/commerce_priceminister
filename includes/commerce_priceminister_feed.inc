<?php

/**
 * @file
 */


/**
 * The Controller for product entities
 */
class CommercePriceministerFeedEntityAPIController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create a default CommerceMarketplaceFeed entity.
   *
   * @param array $values
   *   An array of defaults values to add to the object construction.
   * @return object
   */
  public function create(array $values = array()) {
    global $user;
    $values += array(
      'is_new' => TRUE,
      'id' => NULL,
      'title' => '',
      'local_product_type' => '',
      'local_product_display' => '',
      'priceminister_product_type' => '',
      'fields_mapping' => array(),
      'frequency' => '',
      'uid' => $user->uid,
      'status' => 1,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );
    return parent::create($values);
  }


  /**
   * Saves a feed.
   *
   * @param object $priceminister_product
   *   The full object to save.
   * @param DatabaseTransaction $db_transaction
   *   An optional transaction object.
   *
   * @return bool|int
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   *
   * @throws Exception
   */
  public function save($entity, DatabaseTransaction $db_transaction = NULL) {
    if (!isset($db_transaction)) {
      $db_transaction = db_transaction();
      $started_transaction = TRUE;
    }

    try {
      // Determine if we will be inserting a new feed.
      $entity->is_new = empty($entity->id);
      if ($entity->is_new) {
        // Add required field.
        $required_hidden_fields = CommercePriceministerFeed::getRequiredHiddenFields();
        foreach ($required_hidden_fields as $remote_field => $local_field) {
          if ($entity->getEntityFields('commerce_product', $local_field)) {
            $entity_type = 'commerce_product';
          }
          elseif ($entity->getEntityFields('node', $local_field)) {
            $entity_type = 'commerce_priceminister_product_display';
          }
          else {
            $entity_type = 'commerce_priceminister_product_metadata';
          }

          $drupal_field = array('field' => array($entity_type => $local_field));
          $priceminister_field = $entity->getPriceministerProductTypeField($remote_field);
          if ($local_field && $priceminister_field) {
            $entity->fields_mapping[$remote_field] = array(
              'local' => $drupal_field,
              'priceminister' => array('field' => $priceminister_field),
            );
          }
        }
      }

      // Set the timestamp fields.
      if (empty($entity->created)) {
        $entity->created = REQUEST_TIME;
      }
      else {
        if ($entity->created === '') {
          unset($entity->created);
        }
      }

      $entity->changed = REQUEST_TIME;

      return parent::save($entity, $db_transaction);
    }
    catch (Exception $e) {
      if (!empty($started_transaction)) {
        $db_transaction->rollback();
        watchdog_exception($this->entityType, $e);
      }
      throw $e;
    }
  }

}


class CommercePriceministerFeed extends Entity {

  public function __construct(array $values = array()) {
    parent::__construct($values, 'commerce_priceminister_feed');
  }

  /**
   * Global list of required fields which should not be exposed to user.
   *
   * @return array
   */
  static public function getRequiredHiddenFields() {
    return array(
      'submitterreference' => 'sku',
      'sellerReference' => 'sku',
      'pid' => 'pid',
      'aid' => 'aid',
    );
  }

  /*
   * Get the list of all the available fields for this product.
   *
   * Return all available fields if $required = NULL.
   * Return required fields if $required = TRUE.
   * Rerurn non-required fields if $required = FALSE.
   */
  public function getPriceministerProductTypeFields($condition = NULL) {
    if (!isset($this->priceminister_product_type_fields)) {
      // Get all the availables fields for the product type selected in the
      // configuration of the feed.
      $this->priceminister_product_type_fields = commerce_priceminister_get_priceminister_product_type_fields($this->priceminister_product_type);
    }

    if ($this->priceminister_product_type_fields) {
      $field_mapping = $this->fields_mapping;
      if (is_array($condition)) {
        if (isset($condition['required']) && $condition['required'] == TRUE) {
          foreach ($this->priceminister_product_type_fields as $key => $field) {
            if ($field['mandatory'] == TRUE) {
              $fields[$key] = $field;
            }
          }
        }
        elseif (isset($condition['required']) && $condition['required'] == FALSE) {
          foreach ($this->priceminister_product_type_fields as $key => $field) {
            if ($field['mandatory'] == FALSE) {
              $fields[$key] = $field;
            }
          }
        }
        return $fields;
      }
      return $this->priceminister_product_type_fields;
    }
    return array();
  }


  /*
   * Return a field from the list of fields available for a product type.
   */
  public function getPriceministerProductTypeField($field_name) {
    $field = $this->getPriceministerProductTypeFields();
    if (!empty($field) && isset($field[$field_name])) {
      return $field[$field_name];
    }
  }

  /*
   * Get the list of already mapped fields.
   */
  public function getPriceministerProductTypeMappedFields($mapped = TRUE) {

    $mapped_fields = array();
    foreach ($this->fields_mapping as $key => $mapping) {
      if (isset($mapping['priceminister']['field'])) {
        $mapped_fields[$key] = $mapping['priceminister']['field'];
      }
      elseif (isset($mapping['priceminister']['value'])) {
        $mapped_fields[$key] = $mapping['priceminister']['value'];
      }
    }

    if ($mapped == TRUE) {
      return $mapped_fields;
    }
    else {
      $product_type_fields = $this->getPriceministerProductTypeFields();
      return array_diff_key($product_type_fields, $mapped_fields);
    }
  }

  /**
   * Wrapper around entity_get_property_info to get a list of
   * available fields/properties for the feed's source entity.
   *
   * @param $entity_type
   *   Entity type to get fields informations from.
   * @param null $field
   *   Field to filter results on.
   *
   * @return array
   *   Return a list of availables fields
   *   Array() if $field or bundle_name doesn't exist in this entity type.
   */
  public function getEntityFields($entity_type, $field = NULL) {
    $entity_fields = array();
    $entity_properties = entity_get_property_info($entity_type);
    if (isset($entity_properties['properties'])) {
      $entity_fields = $entity_properties['properties'];
      switch ($entity_type) {
        case 'commerce_product':
          $bundle_name = $this->local_product_type;
          break;
        case 'node':
          $bundle_name = $this->local_product_display;
          break;
        default:
          $bundle_name = NULL;
      }
      if (!empty($bundle_name)) {
        if (isset($entity_properties['bundles'][$bundle_name]['properties'])) {
          $entity_fields += $entity_properties['bundles'][$bundle_name]['properties'];
        }
      }
      else {
        return array();
      }
    }

    if ($field) {
      return isset($entity_fields[$field]) ? $entity_fields[$field] : array();
    }

    return $entity_fields;
  }
}