<?php

class PriceministerProductQueue extends SystemQueue {

  /**
   * Implements createItem().
   *
   * @param $data
   *   Contain the product id.
   * @return bool
   */
  public function createItem($data) {
    $exist = db_query('SELECT item_id FROM {queue} WHERE name = :name AND data = :data AND expire = 0', array(':name' => $this->name, ':data' => $data))->fetchField();
    if (!$exist) {
      $query = db_insert('queue')
        ->fields(array(
          'name' => $this->name,
          'data' => $data,
          // We cannot rely on REQUEST_TIME because many items might be created
          // by a single request which takes longer than 1 second.
          'created' => time(),
        ));
      return (bool) $query->execute();
    }
    return TRUE;
  }

  /**
   * Implements claimItem().
   *
   * @param int $lease_time
   *   TODO: should be greater than cron delay.
   * @return bool
   */
  public function claimItem($lease_time = 30) {
    $item = db_query_range('SELECT data, item_id FROM {queue} q WHERE expire = 0 AND name = :name ORDER BY created ASC', 0, 1, array(':name' => $this->name))->fetchObject();
    if ($item) {
      // Try to update the item. Only one thread can succeed in UPDATEing the
      // same row. We cannot rely on REQUEST_TIME because items might be
      // claimed by a single consumer which runs longer than 1 second. If we
      // continue to use REQUEST_TIME instead of the current time(), we steal
      // time from the lease, and will tend to reset items before the lease
      // should really expire.
      $update = db_update('queue')
        ->fields(array(
          'expire' => time() + $lease_time,
        ))
        ->condition('item_id', $item->item_id)
        ->condition('expire', 0);
      // If there are affected rows, this update succeeded.
      if ($update->execute()) {
        return $item;
      }
    }
    else {
      // No items currently available to claim.
      return FALSE;
    }
  }

  /**
   * Get all available items from queue.
   *
   * @param int $lease_time
   *   TODO: should be greater than cron delay.
   * @return array|bool
   */
  public function claimItems($count, $lease_time = 30) {
    $results = $item = db_query_range('SELECT data, item_id FROM {queue} q WHERE expire = 0 AND name = :name ORDER BY created ASC', 0, $count, array(':name' => $this->name));
    $items = array();
    foreach ($results as $item) {
      $items[$item->item_id] = $item;
    }
    if ($items) {
      $update = db_update('queue')
        ->fields(array(
          'expire' => time() + $lease_time,
        ))
        ->condition('item_id', array_keys($items))
        ->condition('expire', 0);
      // If there are affected rows, this update succeeded.
      if ($update->execute()) {
        return $items;
      }
    }
    else {
      // No items currently available to claim.
      return FALSE;
    }
  }

  public function releaseItems($item_ids) {
    $update = db_update('queue')
      ->fields(array(
        'expire' => 0,
      ))
      ->condition('item_id', $item_ids, 'IN');
    return $update->execute();
  }

  public function deleteItems($item_ids) {
    db_delete('queue')
      ->condition('item_id', $item_ids, 'IN')
      ->execute();
  }

  /**
   * Return the number of items ready to be exported.
   *
   * @return mixed
   */
  public function numberOfAvailableItems() {
    return db_query('SELECT COUNT(item_id) FROM {queue} WHERE name = :name AND expire = 0', array(':name' => $this->name))->fetchField();
  }
}
